<?php

namespace App\Http\Middleware;

use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client = new \GuzzleHttp\Client(['base_uri' => 'http://83.197.226.71:8001/']);

        if(isset($_COOKIE ['tokenCookie'])){
            $token = array(
                'token' => $_COOKIE ['tokenCookie'],
            );
        }else {
            $request->session()->flash('error', 'Vous devez etre connecté pour acceder à cette page');
            return redirect('/error');
        }

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $response = $client->request('POST', 'verifToken',[
            'headers' => $headers,
            'body' => json_encode($token)
        ]);

        $responseDecode = json_decode($response->getBody()->getContents(),true);

        if($responseDecode === 'admin' || $responseDecode === 'formateur' || $responseDecode === 'apprenant'){
            $request->merge(array("statut" => $responseDecode));
            return $next($request);
        }else {
            return redirect('/error');
        }
    }
}
