<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function homeRedirection () {
        return view('home');
    }

    public function dashboard (Request $request) {
        $statutUser = $request->instance()->query('statut');
        if($statutUser === 'admin'){
            return view('dashboard.dashboard');
        }else if ($statutUser === 'formateur') {
            return view('dashboard.dashboard');
        } else if ($statutUser === 'apprenant'){
            return view('dashboard.dashboardTrainee');
        }
    }

    public function company (Request $request) {
        $statutUser = $request->instance()->query('statut');
        if($statutUser === 'admin'){
            return view('company.company');
        }else if ($statutUser === 'formateur') {
            return view('company.companyFormer');
        } else if ($statutUser === 'apprenant'){
            return view('company.companyTrainee');
        }
    }

    public function profile () {
        return view('profile');
    }
}
