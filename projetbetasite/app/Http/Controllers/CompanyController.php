<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function createCompany () {
        $client = new \GuzzleHttp\Client(['base_uri' => 'http://83.197.226.71:8001/']);
        $authArray = array(
            'nom' => $_POST ["nomCompanyCreate"],
            'secteur' => $_POST ["secteurCompanyCreate"],
            'adresse' => $_POST ["adresseCompanyCreate"],
            'telephone' => $_POST ["telephoneCompanyCreate"],
            'departement' => $_POST ["departementCompanyCreate"],
            'ville' => $_POST ["villeCompanyCreate"]
        );

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $response = $client->request('POST', 'entreprise/creer',[
            'headers' => $headers,
            'body' => json_encode($authArray)
        ]);

        return redirect('company');
    }

    public function modifyCompany () {
        $client = new \GuzzleHttp\Client(['base_uri' => 'http://83.197.226.71:8001/']);
        $authArray = array(
            'nom' => $_POST ["nomFormationModify"],
            'tags' => $_POST ["tagsFormationModify"],
            'statut' => $_POST ["statutFormationModify"],
            'date_debut' => $_POST ["dateDebutFormationModify"],
            'date_fin' => $_POST ["dateFinFormationModify"]
        );

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $response = $client->request('POST', 'entreprise/modifier',[
            'headers' => $headers,
            'body' => json_encode($authArray)
        ]);

        return view('dasboard.dashboard');
    }
}
