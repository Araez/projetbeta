<?php

namespace App\Http\Controllers;

class LoginApiController extends Controller
{
    public function loginApi()
    {

            $client = new \GuzzleHttp\Client(['base_uri' => 'http://83.197.226.71:8001']);

            $authArray = array(
                'username' => $_POST ["email"],
                'password' => $_POST ["password"]
            );

            $headers = [
                'Content-Type' => 'application/json'
            ];

            $response = $client->request('POST', '/connect',[
                'timeout' => 3.0,
                '/redirect/hello',
                'headers' => $headers,
                'body' => json_encode($authArray)
            ]);

            $responseDecode = json_decode($response->getBody()->getContents(),true);
            $token = $responseDecode ['token'];
            setcookie('tokenCookie', $token);
            //return view('dashboard.dashboard', ['var' => $token]);
            return redirect('dashboard');
    }

    public function logoutApi () {
        setcookie("tokenCookie","",time()-3600);
        return redirect('/');
    }
}
