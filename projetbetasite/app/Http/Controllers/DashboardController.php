<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function createFormation () {
        $client = new \GuzzleHttp\Client(['base_uri' => 'http://83.197.226.71:8001/']);
        $authArray = array(
            'nom' => $_POST ["nomFormationCreate"],
            'tags' => $_POST ["tagsFormationCreate"],
            'statut' => $_POST ["statutFormationCreate"],
            'date_debut' => $_POST ["dateDebutFormationCreate"],
            'date_fin' => $_POST ["dateFinFormationCreate"]
        );

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $response = $client->request('POST', 'formation/creer',[
            'headers' => $headers,
            'body' => json_encode($authArray)
        ]);

        return redirect('dashboard');
    }

    public function modifyFormation () {
        $client = new \GuzzleHttp\Client(['base_uri' => 'http://83.197.226.71:8001/']);
        $authArray = array(
            'nom' => $_POST ["nomFormationModify"],
            'tags' => $_POST ["tagsFormationModify"],
            'statut' => $_POST ["statutFormationModify"],
            'date_debut' => $_POST ["dateDebutFormationModify"],
            'date_fin' => $_POST ["dateFinFormationModify"]
        );

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $response = $client->request('POST', 'formation/modifier',[
            'headers' => $headers,
            'body' => json_encode($authArray)
        ]);

        return view('dasboard.dashboard');
    }

    public function deleteFormation () {
        $client = new \GuzzleHttp\Client(['base_uri' => 'http://83.197.226.71:8001/']);
        $authArray = array(
            'id' => intval($_POST ["id_company_delete"])
        );

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $response = $client->request('POST', 'formation/supprimer_formation',[
            'headers' => $headers,
            'body' => json_encode($authArray)
        ]);

        return view('dashboard.dashboard');
    }
}
