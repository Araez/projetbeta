<?php

namespace App\Http\Controllers;

class RegisterApiController extends Controller
{
    public function formulaires()
    {
        return view('registerApi');
    }

    public function traitements()
    {
        $client = new \GuzzleHttp\Client(['base_uri' => 'http://10.105.21.36:8001/']);
        $authArray = array(
            'name' => $_POST ["name"],
            'firstname' => $_POST ["firstname"],
            'email' => $_POST ["email"],
            'phone' => $_POST ["phone"],
            'plain_password' => $_POST ["password"]
        );

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $response = $client->request('POST', 'api/register',[
            'headers' => $headers,
            'body' => json_encode($authArray)
        ]);

        return view('home', ['var' => 'Votre compte a bien été créé']);
    }


}
