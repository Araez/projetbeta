$( document ).ready(function() {

    //-------------------------------Modal création de compte-----------------------------------

    $('#myModal').on('show.bs.modal', function (event) {
        $( ".home_container" ).hide();
    })

    $('#myModal').on('hidden.bs.modal', function (event) {
        $( ".home_container" ).show();
    })

    var baseUrl = 'http://83.197.226.71:8001/';


    //------------------------------------Token-------------------------------------
    var arr = {
        'token':  $.cookie("tokenCookie")
    };
    //---------------------------------VerifToken----------------------------------

    $.ajax({
        url: baseUrl + "verifToken",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(arr)
    }).done(function(data){
        var obj = JSON.parse(JSON.stringify(data));
        if(obj === 'admin'){
            //---------------------------------------------------------------Admin------------------------------------------------------------------------------------------
            //----------------------Récupérer les formations

            $.ajax({
                headers: {
                    'X-AUTH-CONTENT': $.cookie("tokenCookie"),
                },
                url: baseUrl + "formation/lister",
                type: 'POST',
                dataType: 'json',
                data: {}
            }).done(function(data){
                var obj = JSON.parse(JSON.stringify(data));
                for(var i = 0; i < obj.length; i++) {
                    var object = obj[i];

                    $("<tr></tr>").appendTo(".dashboard_tbody_administrator").append(
                        "<th scope='row'>" + (i+1) + "</th>"+
                        "<td class='company_list_content'>" + object.nom + "</td>"+
                        "<td class='company_list_content'>" + object.statut + "</td>"+
                        "<td class='company_list_content'>" + object.date_debut + "</td>"+
                        "<td class='company_list_content'>" + object.date_fin + "</td>"+
                        "<td><button type='button' class='btn btn-success' data-toggle='modal' data-target='#formulaire'>Modifier</button></td>"+
                        "<td><button id='" + object.id + "' type='button' class='btn btn-danger btnSuppr' data-toggle='modal' data-target='#modal_dashboard_supprimer_formation'>Supprimer</button></td>"
                    );
                }
                $( ".btnSuppr" ).click(function() {
                    $("#id_company_delete").val($(this).attr("id"));
                });
            });

            //-------------------------Modifier les formations-------------------------------
            $.ajax({
                headers: {
                    'X-AUTH-CONTENT': $.cookie("tokenCookie"),
                },
                url: baseUrl + "formation/lister",
                type: 'POST',
                dataType: 'json',
                data: {}
            }).done(function(data){
                var obj = JSON.parse(JSON.stringify(data));
                for(var i = 0; i < obj.length; i++) {
                    var object = obj[i];

                    $("<tr></tr>").appendTo(".dashboard_tbody_administrator").append(
                        "<th scope='row'>" + (i+1) + "</th>"+
                        "<td class='company_list_content'>" + object.nom + "</td>"+
                        "<td class='company_list_content'>" + object.statut + "</td>"+
                        "<td class='company_list_content'>" + object.date_debut + "</td>"+
                        "<td class='company_list_content'>" + object.date_fin + "</td>"+
                        "<td><button type='button' class='btn btn-success' data-toggle='modal' data-target='#formulaire'>Modifier</button></td>"+
                        "<td><button id='" + object.id + "' type='button' class='btn btn-danger btnSuppr' data-toggle='modal' data-target='#modal_dashboard_supprimer_formation'>Supprimer</button></td>"
                    );
                }
                $( ".btnSuppr" ).click(function() {
                    $("#id_company_delete").val($(this).attr("id"));
                });
            });

        }else if (obj === 'formateur') {
            //---------------------------------------------------------------Formateur------------------------------------------------------------------------------------------
        }else if (obj === 'apprenant') {
            //---------------------------------------------------------------Apprenant------------------------------------------------------------------------------------------
            //---------------------------Récupérer liste des candidatures
            $.ajax({
                url: baseUrl + "candidatures/lister/token",
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(arr)
            }).done(function(data){
                var obj = JSON.parse(JSON.stringify(data));
                for(var i = 0; i < obj.length; i++) {
                    var object = obj[i];

                    $("<tr></tr>").appendTo(".dashboard_tbody_trainee").append(
                        "<th scope='row'>" + (i+1) + "</th>"+
                        "<td class='company_list_content'>" + object.date_candidature + "</td>"+
                        "<td class='company_list_content'>" + object.id_entreprise.nom + "</td>"+
                        "<td class='company_list_content'>" + object.reponse + "</td>"+
                        "<td><button type='button' class='btn btn-success' data-toggle='modal' data-target='#formulaire'>Modifier</button></td>"
                    );
                }
            }).always(function(){

            });
        }
    });


    //--------------------------------Ajax liste entreprise---------------------------------

        $.ajax({
            url: baseUrl + "entreprise/lister",
            type: 'GET',
            dataType: 'json',
            data: {
            }
        }).done(function(data){
            var obj = JSON.parse(JSON.stringify(data));
            for(var i = 0; i < obj.length; i++) {
                var object = obj[i];


                $("<div class='company_list_div'></div>").appendTo(".company_list").append(
                    "<h4 class='company_list_content'>" + object.nom + "</h4>"+
                    "<p class='company_list_content' id='secteur'>" + object.secteur + "</p>"+
                    "<p class='company_list_content' id='adresse'>" + object.adresse + "</p>"+
                    "<p class='company_list_content'>" + object.telephone + "</p>"+
                    "<p class='company_list_content' id='departement'>" + object.departement + "</p>"+
                    "<p class='company_list_content' id='ville'>" + object.ville + "</p>"+
                    "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#modal_company_modify_company' id='" + object.id + "' type='button'>Modifier</button>"
                );
            }
        }).always(function(){

        });
    //-------------------------------- recherche entreprise---------------------------------

    //-------Recherche
    $(".company_search_bar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".company_list_div h4").filter(function () {
            $(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });



    //-------Adresse
    $("#company_adress_search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".test").filter(function () {
            $(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    //-------Ville
   /* $("#company_adress_search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".company_list_div #ville").filter(function () {
            $(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });*/

    //-------Secteur activité
    $("#company_select_secteur").change(function () {
        var value = $('option:selected', this).text();
        $(".company_list_div #secteur").filter(function () {
            $(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    })

    //-------Département
    $("#company_select_departement").change(function () {
        var value = $('option:selected', this).text();
        $(".company_list_div #departement").filter(function () {
            $(this).parent().toggle($(this).text().indexOf(value) > -1)
        });
    })

        //--------------------------------------------------------------------------------

    $.ajax({
        url: baseUrl + "entreprise/rechercher/secteurs",
        type: 'GET',
        dataType: 'json',
        data: {
        }
    }).done(function(data){
        var obj = JSON.parse(JSON.stringify(data));
        for(var i = 0; i < obj.length; i++) {
            var object = obj[i];
            $("#company_select_secteur").append("<option>" + obj [i] + "</option>")
        }
    }).always(function(){

    });

    $.ajax({
        url: baseUrl + "entreprise/rechercher/departements",
        type: 'GET',
        dataType: 'json',
        data: {
        }
    }).done(function(data){
        var obj = JSON.parse(JSON.stringify(data));
        for(var i = 0; i < obj.length; i++) {
            var object = obj[i];
            $("#company_select_departement").append("<option>" + obj [i] + "</option>")
        }
    }).always(function(){

    });

    //-----------------------------------------------------------Dashboard-----------------------------------------------------------------------------------------------

});




