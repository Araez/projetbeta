@extends('layouts.template')

@section('content')
    <!-- DASHBOARD -->

    <div class="dashboard_container">
        <div class="dashboard_container_header">
            <div class="dashboard_container_image">
                <img src="https://www.afpa.fr/image/layout_set_logo?img_id=34521924&t=1582904779220">
            </div>

            <div class="dashboard_container_texts">
                <div><h1>Tableau de bord Stagiaire</h1></div>
                <div class="d-flex">
                    <div class="pr-5"><strong>125</strong> candidatures déposées</div>
                    <div class="pr-5"><strong>20</strong> réponses</div>
                    <div class="pr-5"><strong>105</strong> en attente</div>
                </div>
            </div>
        </div>


        <div class="container dashboard_container_board">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Numéro</th>
                    <th scope="col">Date</th>
                    <th scope="col">Entreprise</th>
                    <th scope="col">Statut</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody class="dashboard_tbody_trainee">
                <!--<tr>
                    <th scope="row">3</th>
                    <td>12/03/2020</td>
                    <td>Le duplex club</td>
                    <td>En attente</td>
                    <td>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#formulaire">
                            Modifier
                        </button>
                    </td>
                </tr>-->
                </tbody>
            </table>
        </div>

    </div>
    <!-- Modal -->
    <div class="modal fade" id="formulaire">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Modifier la candidature</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body row">
                    <form class="col" action="" method="post">
                        <div class="form-group">
                            <label for="date" class="form-control-label">Date</label>
                            <input type="date" class="form-control" name="date" id="date" placeholder="Date">
                        </div>

                        <div class="form-group">
                            <select class="custom-select custom-select-lg mb-3 company_collapse_search_content">
                                <option selected>En attente</option>
                                <option value="1">One</option>
                            </select>
                        </div>

                        <a href="#">Supprimer la candidature</a>

                        <button type="submit" class="btn btn-primary pull-right">Modifier</button>
                    </form>

                </div>
            </div>

        </div>
    </div>
@endsection
