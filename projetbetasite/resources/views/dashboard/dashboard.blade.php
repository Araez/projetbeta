@extends('layouts.template')

@section('content')
        <!-- DASHBOARD -->

        <div class="dashboard_container">
            <div class="dashboard_container_header">
                <div class="dashboard_container_image">
                    <img src="https://www.afpa.fr/image/layout_set_logo?img_id=34521924&t=1582904779220">
                </div>

                <div class="dashboard_container_texts">
                    <div><h1>Tableau de bord Administrateur</h1></div>
                </div>
            </div>

            <div>
                <button type='button' class='btn btn-success' data-toggle='modal' data-target='#modal_dashboard_ajouter_formation'>Ajouter une formation</button>
            </div>

            <div class="container dashboard_container_board">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Numéro</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Statut</th>
                        <th scope="col">Date de début</th>
                        <th scope="col">Date de Fin</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="dashboard_tbody_administrator">
                    </tbody>
                </table>
            </div>

        </div>
        <!-- Modal Modifier une formation -->
        <div class="modal fade" id="formulaire">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5>Modifier une formation</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body row">
                        <form class="col" action="{{route('modifyFormation')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <p>Nom de la formation:</p>
                                <input type="text" name="nomFormationModify" id="">
                                <p>tags:</p>
                                <input type="text" name="tagsFormationModify" id="">
                                <p>statut:</p>
                                <select name="statutFormationModify" id="">
                                    <option selected value="En cours">En cours</option>
                                    <option value="Terminée">Terminée</option>
                                </select>
                                <p>Date de début:</p>
                                <input type="date" name="dateDebutFormationModify" id="">
                                <p>date de Fin:</p>
                                <input type="date" name="dateFinFormationModify" id="">
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Modifier la formation</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>


        <!-- Modal Ajouter une formation-->
        <div class="modal fade" id="modal_dashboard_ajouter_formation">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5>Créer une formation</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body row">
                        <form class="col" action="{{route('createFormation')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <p>Nom de la formation:</p>
                                <input type="text" name="nomFormationCreate" id="">
                                <p>tags:</p>
                                <input type="text" name="tagsFormationCreate" id="">
                                <p>statut:</p>
                                <select name="statutFormationCreate" id="">
                                    <option selected value="En cours">En cours</option>
                                    <option value="Terminée">Terminée</option>
                                </select>
                                <p>Date de début:</p>
                                <input type="date" name="dateDebutFormationCreate" id="">
                                <p>date de Fin:</p>
                                <input type="date" name="dateFinFormationCreate" id="">
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Ajouter la formation</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>

        <!-- Modal Supprimer une formation-->
        <div class="modal fade" id="modal_dashboard_supprimer_formation">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5>Supprimer une formation</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body row">
                        <form class="col" action="{{route('deleteFormation')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <p>Voulez vous vraiment supprimer cette formation ?</p>
                                <input hidden type="text" name="id_company_delete" id="id_company_delete">
                                <button type="submit" class="bt btn-danger">Oui</button>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
@endsection
