
<button class="company_list_content" type="button" data-toggle="modal" data-target="#modal_add_company">Ajouter une entreprise</button>

<div class="container">
    <div class="modal fade" id="modal_add_company">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Ajouter une entreprise</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form class="former_form" action="" method="post">
                        <label class="former_form_content" for="">Nom de l'entreprise:</label>
                        <input class="former_form_content" type="text">
                        <label class="former_form_content" for="">Numéro de téléphone:</label>
                        <input class="former_form_content" type="number">
                        <label class="former_form_content" for="">Email:</label>
                        <input class="former_form_content" type="email">
                        <label class="former_form_content" for="">Adresse:</label>
                        <input class="former_form_content" type="text">
                        <button class="btn btn-primary former_form_content" type="button" data-dismiss="modal">Ajouter</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
