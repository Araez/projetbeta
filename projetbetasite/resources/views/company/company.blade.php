@extends('layouts.template')

@section('content')
    <button id="bouton_create_company" type='button' class='btn btn-success' data-toggle='modal' data-target='#modal_company_add_company'>+</button>
    <div class="company_container">
        <form class="company_form">
            <input class="form-control company_search_bar" placeholder="Rechercher une entreprise.." type="search" name="">
          <!--  <input class="btn btn-success" type="submit" value="Rechercher">-->
        </form>




        <div class="container company_container_collapse">
            <a id="company_link_search" data-toggle="collapse" data-target="#company_collapse_search">Recherche avancée</a>
            <div id="company_collapse_search" class="collapse company_collapse_container">
                <select id="company_select_secteur" class="custom-select custom-select-lg mb-3 company_collapse_search_content">
                    <option selected>Secteur d'activité</option>
                </select>

                <select id="company_select_departement" class="custom-select custom-select-lg mb-3 company_collapse_search_content">
                    <option selected>Département</option>
                </select>

                <div id="company_filter_distance" class="company_collapse_search_content">
                    <input id="company_adress_search" class="form-control" type="search" placeholder="Ville, adresse.." name="">
                    <input class="form-control" type="number" placeholder="Distance en km" name="" id="">
                </div>
            </div>
        </div>


        <div class="company_list">

        </div>




    <div class="container">
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Afpa</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Modal body..
                    </div>
                </div>
            </div>
        </div>



        <!-- Modal Ajouter une entreprise-->
        <div class="modal fade" id="modal_company_add_company">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5>Créer une entreprise</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body row">
                        <form class="col" action="{{route('createCompany')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <p>Nom de l'entreprise:</p>
                                <input type="text" name="nomCompanyCreate" id="">
                                <p>Secteur:</p>
                                <input type="text" name="secteurCompanyCreate" id="">
                                <p>Adresse:</p>
                                <input type="text" name="adresseCompanyCreate" id="">
                                <p>Telephone:</p>
                                <input type="tel" name="telephoneCompanyCreate" id="">
                                <p>Département:</p>
                                <input type="text" name="departementCompanyCreate" id="">
                                <p>Ville:</p>
                                <input type="text" name="villeCompanyCreate" id="">
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Ajouter l'entreprise</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>

        <!-- Modal Modifier une entreprise-->
        <div class="modal fade" id="modal_company_modify_company">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5>Modifier une entreprise</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body row">
                        <form class="col" action="{{route('createCompany')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <p>Nom de l'entreprise:</p>
                                <input type="text" name="nomCompanyCreate" id="">
                                <p>Secteur:</p>
                                <input type="text" name="secteurCompanyCreate" id="">
                                <p>Adresse:</p>
                                <input type="text" name="adresseCompanyCreate" id="">
                                <p>Telephone:</p>
                                <input type="tel" name="telephoneCompanyCreate" id="">
                                <p>Département:</p>
                                <input type="text" name="departementCompanyCreate" id="">
                                <p>Ville:</p>
                                <input type="text" name="villeCompanyCreate" id="">
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Modifier l'entreprise</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>

@endsection
