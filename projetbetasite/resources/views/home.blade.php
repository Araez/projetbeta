
@extends('layouts.templateDisconnected')

@section('content')
    <div class="home_container">
        <img class="img-fluid home_logo" src="{{asset('images/logo2.png')}}" alt="Responsive image">

        <form class="home_form_login" method="POST" action="{{ route('login') }}">
            @csrf
            <p class="home_form_login_content">Email:</p>
            <input class="form-control home_form_login_content" type="email" name="email">
            <p class="home_form_login_content">Mot de passe:</p>
            <input class="form-control home_form_login_content" type="password" name="password">
            <input class="btn btn-success home_form_login_content" id="home_login_button" type="submit" value="Se connecter">
            <a class="home_form_login_reset" href="{{url('password/reset')}}">Mot de passe oublié</a>
            <div class="home_form_login_create home_form_login_content">
                <p>Pas encore inscrit ?</p>
               <!-- <a href="{{route('register')}}">créer un compte</a>-->
                <a id="home_create_button" href="#myModal" data-toggle="modal" data-target="#myModal">Créer un compte</a>
            </div>
        </form>

    </div>
    <div class="container">


        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Créer un compte</h4>
                        <button id="home_close_modal" type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form class="home_form_login" method="POST" action="{{ route('register') }}">
                            @csrf
                            <p class="home_form_login_content">Nom:</p>
                            <input class="form-control home_form_login_content" type="text" name="name">
                            <p class="home_form_login_content">Prénom:</p>
                            <input class="form-control home_form_login_content" type="text" name="firstname">
                            <p class="home_form_login_content">Email:</p>
                            <input class="form-control home_form_login_content" type="email" name="email">
                            <p class="home_form_login_content">Téléphone:</p>
                            <input class="form-control home_form_login_content" type="tel" name="phone">
                            <p class="home_form_login_content">Mot de passe:</p>
                            <input class="form-control home_form_login_content" type="password" name="password">
                            <input class="btn btn-success home_form_login_content" id="home_login_button" type="submit" value="Créer le compte">
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
