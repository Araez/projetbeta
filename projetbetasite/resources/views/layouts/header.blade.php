<nav class="navbar navbar-expand-md header_navbar"><!--navbar-dark-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav header_list_links">
            <li class="nav-item">
                <a class="nav-link header_links" href="{{route('dashboard')}}">Tableau de bord</a>
            </li>

            <li class="nav-item">
                <a class="nav-link header_links" href="{{url('company')}}">Entreprise</a>
            </li>

            <li class="nav-item">

            </li>
        </ul>

        <ul class="navbar-nav header_list_profile">
            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link dropdown-toggle header_links" href="" data-toggle="dropdown">Mon compte</a>
                    <div class="dropdown-menu dropdownLogin">
                                <a class="nav-link" href="{{ url('profile') }}">Profile</a>
                        <form method="post" action="{{route('logout')}}">
                            @csrf
                            <input class="submit-link" type="submit" value="Se déconnecter">
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>
