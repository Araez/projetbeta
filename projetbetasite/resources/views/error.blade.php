@extends('layouts.templateDisconnected')

@section('content')
    <div class="home_container">
        @if (session('error'))
            {{ session('error') }}
            <a href="{{url('/')}}">Retourner à la page de connexion</a>
        @endif
    </div>

@endsection
