<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@homeRedirection')
    ->name('home');

Route::post('/login', 'LoginApiController@loginApi')
->name('login');

Route::post('/logout', 'LoginApiController@logoutApi')
    ->name('logout');

Route::get('/register', 'RegisterApiController@formulaires')
    ->name('register');

Route::post('/register', 'RegisterApiController@traitements')
    ->name('register');

Route::get('/error', 'ErrorController@Error')
    ->name('verify');

Route::get('/profile', 'ProfleController@giveProfile')
    ->name('giveprofile');

Route::get('/profile', 'ProfleController@giveProfile')
    ->name('modifyProfile');

Route::get('/profile', 'ProfleController@giveProfile')
    ->name('giveprofile');

Route::post('/createFormation', 'DashboardController@createFormation')
    ->name('createFormation');

Route::post('/modifyFormation', 'DashboardController@modifyFormation')
    ->name('modifyFormation');

Route::post('/deleteFormation', 'DashboardController@deleteFormation')
    ->name('deleteFormation');

Route::post('/createCompany', 'CompanyController@createCompany')
    ->name('createCompany');

Route::post('/modifyCompany', 'CompanyController@modifyCompany')
    ->name('modifyCompany');

Route::get('/dashboard', 'IndexController@dashboard')->middleware('App\Http\Middleware\CheckToken')
    ->name('dashboard');
Route::get('/company', 'IndexController@company')->middleware('App\Http\Middleware\CheckToken')
    ->name('company');
Route::get('/profile', 'IndexController@profile')->middleware('App\Http\Middleware\CheckToken')
    ->name('profile');




