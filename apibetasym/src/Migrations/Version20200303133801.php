<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200303133801 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD api_token VARCHAR(255) DEFAULT NULL, CHANGE plain_password plain_password VARCHAR(255) ');
        $this->addSql('ALTER TABLE formateur ADD api_token VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin ADD api_token VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE apprenant ADD api_token VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE admin DROP api_token');
        $this->addSql('ALTER TABLE apprenant DROP api_token');
        $this->addSql('ALTER TABLE formateur DROP api_token');
        $this->addSql('ALTER TABLE user DROP api_token, CHANGE plain_password plain_password VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
