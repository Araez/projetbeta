<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200304121011 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD roles VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE formateur ADD roles VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin ADD roles VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE apprenant ADD roles VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE admin DROP roles');
        $this->addSql('ALTER TABLE apprenant DROP roles');
        $this->addSql('ALTER TABLE formateur DROP roles');
        $this->addSql('ALTER TABLE user DROP roles');
    }
}
