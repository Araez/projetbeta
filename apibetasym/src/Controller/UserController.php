<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

// /**
//      * @Route("/api", name="api", methods={"GET", "POST"})
//      */

class UserController extends AbstractController
{
    /**
     * @Route("/create", name="register2_user", methods={"POST"})
     */
    public function register_user(Request $request, EntityManagerInterface $manager, SerializerInterface $serializer, ValidatorInterface $validator, UserPasswordEncoderInterface $encoder)
    {
       
        $data = $request->getContent();
        
        $user = $serializer->deserialize($data,User::class,'json');
        $haspass= $encoder->encodePassword($user,$user->getPlainPassword());
        $user->setPassword($haspass);
        $user->setPlainPassword("");
        
        //gestion des erreurs de validation
        $errors =  $validator->validate($user);
        if(count($errors)){
            $errorJson = $serializer->serialize($errors,'json');
            return new JsonResponse($errorJson,Response::HTTP_BAD_REQUEST,[],true);

        }else{
            $manager->persist($user);
            $manager->flush();
            return new JsonResponse("ajouté",Response::HTTP_CREATED,[
            ],true);            
        }
    }

     /**
     * @Route("admin/list", name="list_users", methods={"POST"})
     */
    public function index(UserRepository $repository,SerializerInterface $serializer) 
    {
        $elements = $repository->findAll();
        $resultat = $serializer->serialize(
            $elements,
            'json',
            [
            ]
        );
        return new JsonResponse($resultat,200,[],true);
    }

    /**
     * @Route("api/login_check", name="api_login", methods={"POST"})
     */
    public function login(EntityManagerInterface $manager, Request $request, SerializerInterface $serializer)
    {
        
        $user=$this->getUser();
        
        $apiToken = $this->genererToken($user);
        
        $user->setToken($apiToken); // je set un token au user
        
        $manager->flush($user);       // j'update le token du user

        return new JsonResponse([
            'token' => $apiToken
            
        ]);
    }


    // /**
    //  * @Route("/connect", name="connect_user", methods={"POST", "GET"})
    //  */

    
    // public function connect_user(Request $request,UserRepository $userRepository, SerializerInterface $serializer, DenormalizerInterface $denormalizer)
    // {
    //     $data = $request->getContent();

    //     $user = $serializer->deserialize($data,User::class,'json');
            
    //     $log=$user->getEmail();
    //     $pass=$user->getPassword();

    //     $search=$userRepository->findBy(["Email"=>$log]);
       
    //     $user_verif = new User();

        


    //     dd($user_verif);
    //     if (password_verify($pass,$search->getPassword)){

    //     }
    //     dd(password_verify($pass,$search->getPassword));
        
    //     if ($search){
    //         return new JsonResponse("connected",Response::HTTP_OK,[
    //         ],true);    
        
    //     }else{
    //         return new JsonResponse("user not found",Response::HTTP_BAD_REQUEST,[
    //         ],true);    
    //         }
        
    // }

    /**
     * @Route("formateur/validate", name="register_user", methods={"POST"})
     * @Security("has_role('ROLE_FORMATEUR')")
     */
    public function validate(){

    }

    /**
     *
     * @Route ("/account", name ="api_account", methods={"POST", "GET"})
     */
    public function accountApi(){
         if ($user = $this->getUser()){
           return $this->json($user,200,[],['groups' => ['account']]);
         }else {
            return new JsonResponse("",Response::HTTP_BAD_REQUEST,[],false);
        }
    }

   
    private function genererToken()
    {
        
        $random = random_bytes(10);

        return $random;
    }
    
}
