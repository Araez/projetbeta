<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatureRepository")
 */
class Candidature
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Apprenant", inversedBy="candidatures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Id_apprenant;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Date_candidature;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Reponse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Moyen_candidature;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise", inversedBy="candidatures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Id_entreprise;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdApprenant(): ?Apprenant
    {
        return $this->Id_apprenant;
    }

    public function setIdApprenant(?Apprenant $Id_apprenant): self
    {
        $this->Id_apprenant = $Id_apprenant;

        return $this;
    }

    public function getDateCandidature(): ?\DateTimeInterface
    {
        return $this->Date_candidature;
    }

    public function setDateCandidature(?\DateTimeInterface $Date_candidature): self
    {
        $this->Date_candidature = $Date_candidature;

        return $this;
    }

    public function getReponse(): ?string
    {
        return $this->Reponse;
    }

    public function setReponse(?string $Reponse): self
    {
        $this->Reponse = $Reponse;

        return $this;
    }

    public function getMoyenCandidature(): ?string
    {
        return $this->Moyen_candidature;
    }

    public function setMoyenCandidature(string $Moyen_candidature): self
    {
        $this->Moyen_candidature = $Moyen_candidature;

        return $this;
    }

    public function getIdEntreprise(): ?Entreprise
    {
        return $this->Id_entreprise;
    }

    public function setIdEntreprise(?Entreprise $Id_entreprise): self
    {
        $this->Id_entreprise = $Id_entreprise;

        return $this;
    }
}
