<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation 
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Apprenant", inversedBy="formations")
     */
    private $Id_apprenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formateur", inversedBy="formations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Id_formateur;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Tags;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Statut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Date_debut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Date_fin;

    public function __construct()
    {
        $this->Id_apprenant = new ArrayCollection();
    }

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    /**
     * @return Collection|Apprenant[]
     */
    public function getIdApprenant(): Collection
    {
        return $this->Id_apprenant;
    }

    public function addIdApprenant(Apprenant $idApprenant): self
    {
        if (!$this->Id_apprenant->contains($idApprenant)) {
            $this->Id_apprenant[] = $idApprenant;
        }

        return $this;
    }

    public function removeIdApprenant(Apprenant $idApprenant): self
    {
        if ($this->Id_apprenant->contains($idApprenant)) {
            $this->Id_apprenant->removeElement($idApprenant);
        }

        return $this;
    }

    public function getIdFormateur(): ?Formateur
    {
        return $this->Id_formateur;
    }

    public function setIdFormateur(?Formateur $Id_formateur): self
    {
        $this->Id_formateur = $Id_formateur;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->Tags;
    }

    public function setTags(?string $Tags): self
    {
        $this->Tags = $Tags;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->Statut;
    }

    public function setStatut(?string $Statut): self
    {
        $this->Statut = $Statut;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->Date_debut;
    }

    public function setDateDebut(\DateTimeInterface $Date_debut): self
    {
        $this->Date_debut = $Date_debut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->Date_fin;
    }

    public function setDateFin(?\DateTimeInterface $Date_fin): self
    {
        $this->Date_fin = $Date_fin;

        return $this;
    }

   
}
